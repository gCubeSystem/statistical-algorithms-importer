package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;
import java.util.ArrayList;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.input.GlobalVariables;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.input.InterpreterInfo;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.input.ProjectInfo;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.input.InputOutputVariables;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class InputData implements Serializable {

	private static final long serialVersionUID = -2405068429998054485L;
	private ProjectInfo projectInfo;
	private InterpreterInfo interpreterInfo;
	private ArrayList<GlobalVariables> listGlobalVariables;
	private ArrayList<InputOutputVariables> listInputOutputVariables;

	public InputData() {
		super();
	}

	public InputData(ProjectInfo projectInfo, InterpreterInfo interpreterInfo,
			ArrayList<GlobalVariables> listGlobalVariables,
			ArrayList<InputOutputVariables> listInputOutputVariables) {
		super();
		this.projectInfo = projectInfo;
		this.interpreterInfo = interpreterInfo;
		this.listGlobalVariables = listGlobalVariables;
		this.listInputOutputVariables = listInputOutputVariables;
	}

	public ProjectInfo getProjectInfo() {
		return projectInfo;
	}

	public void setProjectInfo(ProjectInfo projectInfo) {
		this.projectInfo = projectInfo;
	}

	public InterpreterInfo getInterpreterInfo() {
		return interpreterInfo;
	}

	public void setInterpreterInfo(InterpreterInfo interpreterInfo) {
		this.interpreterInfo = interpreterInfo;
	}

	public ArrayList<GlobalVariables> getListGlobalVariables() {
		return listGlobalVariables;
	}

	public void setListGlobalVariables(
			ArrayList<GlobalVariables> listGlobalVariables) {
		this.listGlobalVariables = listGlobalVariables;
	}

	public ArrayList<InputOutputVariables> getListInputOutputVariables() {
		return listInputOutputVariables;
	}

	public void setListInputOutputVariables(
			ArrayList<InputOutputVariables> listInputOutputVariables) {
		this.listInputOutputVariables = listInputOutputVariables;
	}

	@Override
	public String toString() {
		return "InputData [projectInfo=" + projectInfo + ", interpreterInfo="
				+ interpreterInfo + ", listGlobalVariables="
				+ listGlobalVariables + ", listInputOutputVariables="
				+ listInputOutputVariables + "]";
	}

}
