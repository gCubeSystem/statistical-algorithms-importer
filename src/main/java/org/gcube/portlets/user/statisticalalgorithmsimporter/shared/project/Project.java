package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class Project implements Serializable {

	private static final long serialVersionUID = -7906477664944910362L;

	private ProjectFolder projectFolder;
	private ProjectConfig projectConfig;
	private MainCode mainCode;
	private InputData inputData;
	private ProjectTarget projectTarget;

	public Project() {
		super();
	}

	public Project(ProjectFolder projectFolder, ProjectConfig projectConfig) {
		super();
		this.projectFolder = projectFolder;
		this.projectConfig = projectConfig;

	}

	public ProjectConfig getProjectConfig() {
		return projectConfig;
	}

	public void setProjectConfig(ProjectConfig projectConfig) {
		this.projectConfig = projectConfig;
	}

	public MainCode getMainCode() {
		return mainCode;
	}

	public void setMainCode(MainCode mainCode) {
		this.mainCode = mainCode;
	}

	public ProjectFolder getProjectFolder() {
		return projectFolder;
	}

	public void setProjectFolder(ProjectFolder projectFolder) {
		this.projectFolder = projectFolder;
	}

	public InputData getInputData() {
		return inputData;
	}

	public void setInputData(InputData inputData) {
		this.inputData = inputData;
	}

	public ProjectTarget getProjectTarget() {
		return projectTarget;
	}

	public void setProjectTarget(ProjectTarget projectTarget) {
		this.projectTarget = projectTarget;
	}

	@Override
	public String toString() {
		return "Project [projectFolder=" + projectFolder + ", projectConfig=" + projectConfig + ", mainCode=" + mainCode
				+ ", inputData=" + inputData + ", projectTarget=" + projectTarget + "]";
	}

}
