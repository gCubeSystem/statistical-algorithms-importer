package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectShareInfo implements Serializable {
	private static final long serialVersionUID = 1153592451993798163L;
	private boolean privateAlgorithm;
	private ArrayList<String> users;

	public ProjectShareInfo() {
		super();
	}

	public ProjectShareInfo(boolean privateAlgorithm, ArrayList<String> users) {
		super();
		this.privateAlgorithm = privateAlgorithm;
		this.users = users;
	}

	public boolean isPrivateAlgorithm() {
		return privateAlgorithm;
	}

	public void setPrivateAlgorithm(boolean privateAlgorithm) {
		this.privateAlgorithm = privateAlgorithm;
	}

	public ArrayList<String> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<String> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "ProjectShareInfo [privateAlgorithm=" + privateAlgorithm + ", users=" + users + "]";
	}

}
