package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectCompile implements Serializable {

	private static final long serialVersionUID = 4603210015575691417L;
	private ItemDescription folder;
	private ItemDescription codeSource;
	private ItemDescription integrationInfo;
	private ItemDescription codeJar;

	public ProjectCompile() {
		super();
	}

	public ProjectCompile(ItemDescription folder) {
		super();
		this.folder = folder;
	}

	public ItemDescription getFolder() {
		return folder;
	}

	public void setFolder(ItemDescription folder) {
		this.folder = folder;
	}

	public ItemDescription getCodeSource() {
		return codeSource;
	}

	public void setCodeSource(ItemDescription codeSource) {
		this.codeSource = codeSource;
	}

	public ItemDescription getCodeJar() {
		return codeJar;
	}

	public void setCodeJar(ItemDescription codeJar) {
		this.codeJar = codeJar;
	}

	public ItemDescription getIntegrationInfo() {
		return integrationInfo;
	}

	public void setIntegrationInfo(ItemDescription integrationInfo) {
		this.integrationInfo = integrationInfo;
	}

	@Override
	public String toString() {
		return "ProjectCompile [folder=" + folder + ", codeSource="
				+ codeSource + ", integrationInfo=" + integrationInfo
				+ ", codeJar=" + codeJar + "]";
	}


}
