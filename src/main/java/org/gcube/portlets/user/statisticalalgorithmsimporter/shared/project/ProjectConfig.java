package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectConfig implements Serializable {

	private static final long serialVersionUID = 8006265995411482998L;
	private String language;
	private ProjectSupport projectSupport;

	public ProjectConfig() {
		super();
	}

	public ProjectConfig(String language, ProjectSupport projectSupport) {
		super();
		this.language = language;
		this.projectSupport = projectSupport;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public ProjectSupport getProjectSupport() {
		return projectSupport;
	}

	public void setProjectSupport(ProjectSupport projectSupport) {
		this.projectSupport = projectSupport;
	}

	@Override
	public String toString() {
		return "ProjectConfig [language=" + language + ", projectSupport=" + projectSupport + "]";
	}

}
