package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class MainCode implements Serializable {

	private static final long serialVersionUID = 6328159797277211090L;
	private ItemDescription itemDescription;

	public MainCode() {
		super();
	}

	public MainCode(ItemDescription itemDescription) {
		super();
		this.itemDescription = itemDescription;
	}

	public ItemDescription getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(ItemDescription itemDescription) {
		this.itemDescription = itemDescription;
	}

	@Override
	public String toString() {
		return "MainCode [itemDescription=" + itemDescription + "]";
	}

}
