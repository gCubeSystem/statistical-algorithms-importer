package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectSupportBlackBox implements ProjectSupport {

	private static final long serialVersionUID = -5337735100116549017L;

	private ItemDescription binaryItem;

	public ProjectSupportBlackBox() {
		super();
	}

	/**
	 * 
	 * @param binaryItem
	 *            Binary Item
	 */
	public ProjectSupportBlackBox(ItemDescription binaryItem) {
		super();
		this.binaryItem = binaryItem;

	}

	public ItemDescription getBinaryItem() {
		return binaryItem;
	}

	public void setBinaryItem(ItemDescription binaryItem) {
		this.binaryItem = binaryItem;
	}

	@Override
	public String toString() {
		return "ProjectSupportBlackBox [binaryItem=" + binaryItem + "]";
	}

}
