package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectDeploy implements Serializable {

	private static final long serialVersionUID = -3403842661465451760L;
	private ItemDescription folder;
	private ItemDescription packageProject;
	private ItemDescription codeJar;

	public ProjectDeploy() {
		super();
	}

	public ProjectDeploy(ItemDescription folder) {
		super();
		this.folder = folder;
	}

	public ItemDescription getFolder() {
		return folder;
	}

	public void setFolder(ItemDescription folder) {
		this.folder = folder;
	}

	public ItemDescription getPackageProject() {
		return packageProject;
	}

	public void setPackageProject(ItemDescription packageProject) {
		this.packageProject = packageProject;
	}

	public ItemDescription getCodeJar() {
		return codeJar;
	}

	public void setCodeJar(ItemDescription codeJar) {
		this.codeJar = codeJar;
	}

	@Override
	public String toString() {
		return "ProjectDeploy [folder=" + folder + ", packageProject="
				+ packageProject + ", codeJar=" + codeJar + "]";
	}

}
