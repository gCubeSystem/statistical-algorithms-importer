package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectFolder implements Serializable {

	private static final long serialVersionUID = 3698120963507381801L;
	private ItemDescription folder;

	public ProjectFolder() {
		super();
	}

	public ProjectFolder(ItemDescription folder) {
		super();
		this.folder = folder;
	}

	public ItemDescription getFolder() {
		return folder;
	}

	public void setFolder(ItemDescription folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "ProjectFolder [folder=" + folder + "]";
	}

}
