package org.gcube.portlets.user.statisticalalgorithmsimporter.client.project;

import org.gcube.portlets.user.statisticalalgorithmsimporter.client.create.ProjectCreateWizard;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.event.NewCodeEvent;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.event.ProjectStatusEvent;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.event.SessionExpiredEvent;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.event.WorkAreaEvent;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.monitor.MonitorDeployOperation;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.monitor.MonitorDeployOperationEvent;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.monitor.MonitorDeployOperationEvent.MonitorDeployOperationEventHandler;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.monitor.StatAlgoImporterMonitor;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.rpc.StatAlgoImporterServiceAsync;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.type.ProjectStatusEventType;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.type.SessionExpiredType;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.type.WorkAreaEventType;
import org.gcube.portlets.user.statisticalalgorithmsimporter.client.utils.UtilsGXT3;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.Constants;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.descriptor.SAIDescriptor;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.exception.StatAlgoImporterSessionExpiredException;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project.InputData;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project.Project;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project.ProjectCreateSession;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project.ProjectSupportBashEdit;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project.ProjectSupportBlackBox;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;
import org.gcube.portlets.widgets.githubconnector.client.GitHubConnectorWizard;
import org.gcube.portlets.widgets.githubconnector.client.wizard.event.WizardEvent;
import org.gcube.portlets.widgets.wsexplorer.client.notification.WorkspaceExplorerSelectNotification.WorskpaceExplorerSelectNotificationListener;
import org.gcube.portlets.widgets.wsexplorer.client.select.WorkspaceExplorerSelectDialog;
import org.gcube.portlets.widgets.wsexplorer.shared.Item;
import org.gcube.portlets.widgets.wsexplorer.shared.ItemType;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.core.client.dom.XDOM;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent.DialogHideHandler;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectManager {
	private EventBus eventBus;
	private Project project;
	private SAIDescriptor saiDescriptor;

	public ProjectManager(EventBus eventBus) {
		this.eventBus = eventBus;
		// bind();
	}

	public void bind() {
		eventBus.addHandler(ProjectStatusEvent.TYPE, new ProjectStatusEvent.ProjectStatusEventHandler() {

			@Override
			public void onProjectStatus(ProjectStatusEvent event) {
				Log.debug("Project Manager catch ProjectStatusEvent:" + event);
				// doProjectStatusManage(event);

			}

		});

	}

	public void createProject() {

		// Example
		WizardEvent.WizardEventHandler handler = new WizardEvent.WizardEventHandler() {

			@Override
			public void onResponse(WizardEvent event) {
				if (event == null || event.getWizardEventType() == null) {
					GWT.log("ProjectCreateWizard Response: null");
					return;
				} else {
					GWT.log("ProjectCreateWizard Response: " + event.getWizardEventType());
					switch (event.getWizardEventType()) {

					case Completed:

						ProjectCreateWizard wizard = (ProjectCreateWizard) event.getSource();
						ProjectCreateSession projectCreateSession = wizard.getProjectCreateSession();
						createProjectOnServer(projectCreateSession);

						break;
					case Background:
					case Aborted:
					case Failed:
						break;
					default:
						break;

					}
				}

			}

		};

		if (Constants.DEBUG_MODE) {
			project = Constants.TEST_PROJECT;
			fireWorkAreaSetupEvent();
		} else {
			final ProjectCreateWizard wizard = new ProjectCreateWizard();
			wizard.addWizardEventHandler(handler);
			wizard.setZIndex(XDOM.getTopZIndex());
			wizard.show();
		}
	}

	public void openProject() {
		WorkspaceExplorerSelectDialog wselectDialog = new WorkspaceExplorerSelectDialog("Select Project Folder", true);

		WorskpaceExplorerSelectNotificationListener handler = new WorskpaceExplorerSelectNotificationListener() {

			@Override
			public void onSelectedItem(Item item) {

				if (item.getType() == ItemType.FOLDER || item.getType() == ItemType.PRIVATE_FOLDER
						|| item.getType() == ItemType.SHARED_FOLDER || item.getType() == ItemType.VRE_FOLDER) {
					openProjectOnServer(item);

				} else {
					UtilsGXT3.info("Attention", "Select a valid project folder!");
				}

			}

			@Override
			public void onFailed(Throwable throwable) {
				Log.error("Error in create project: " + throwable.getLocalizedMessage());
				UtilsGXT3.alert("Error", throwable.getLocalizedMessage());
				throwable.printStackTrace();
			}

			@Override
			public void onAborted() {

			}

			@Override
			public void onNotValidSelection() {
				UtilsGXT3.info("Attention", "Select a valid project folder!");
			}
		};

		wselectDialog.addWorkspaceExplorerSelectNotificationListener(handler);
		wselectDialog.setZIndex(XDOM.getTopZIndex());
		wselectDialog.show();

	}

	public void addGitHubProject() {
		if (project != null) {
			if (project.getProjectFolder() != null && project.getProjectFolder().getFolder() != null) {
				WizardEvent.WizardEventHandler handler = new WizardEvent.WizardEventHandler() {

					@Override
					public void onResponse(WizardEvent event) {
						GWT.log("Wizard Response: " + event.getWizardEventType());
						if (event.getWizardEventType() == null) {
							fireProjectStatusAddResourceEvent();
							return;

						}

						switch (event.getWizardEventType()) {
						case Aborted:
						case Background:
						case Completed:
						case Failed:
						default:
							fireProjectStatusAddResourceEvent();

						}

					}
				};

				GitHubConnectorWizard wizard = new GitHubConnectorWizard(
						project.getProjectFolder().getFolder().getId());
				wizard.addWizardEventHandler(handler);
				wizard.setZIndex(XDOM.getTopZIndex());
				wizard.show();
			} else {
				Log.error("Invalid ProjectFolder: " + project.getProjectFolder());
				UtilsGXT3.alert("Error", "Error retrieving project folder!");
			}

		} else {
			Log.error("Invalid Project: " + project);
			UtilsGXT3.alert("Error", "Error retrieving project: " + project);
		}

	}

	public void addResource() {
		/*
		 * List<ItemType> selectableTypes = new ArrayList<ItemType>();
		 * selectableTypes.add(ItemType.EXTERNAL_FILE);
		 * 
		 * List<ItemType> showableTypes = new ArrayList<ItemType>();
		 * showableTypes.addAll(Arrays.asList(ItemType.values()));
		 */

		WorkspaceExplorerSelectDialog wselectDialog = new WorkspaceExplorerSelectDialog("Select a file resource", false);

		WorskpaceExplorerSelectNotificationListener handler = new WorskpaceExplorerSelectNotificationListener() {

			@Override
			public void onSelectedItem(Item item) {
				if (item == null || item.getType() == ItemType.FOLDER || item.getType() == ItemType.PRIVATE_FOLDER
						|| item.getType() == ItemType.SHARED_FOLDER || item.getType() == ItemType.VRE_FOLDER) {
					UtilsGXT3.info("Attention", "Select a valid file resource!");
				} else {
					String filename = item.getName();
					if (filename != null && !filename.isEmpty()) {
						addResourceToProject(item);
					} else {
						UtilsGXT3.info("Attention", "Select a valid file resource!");
					}
				}

			}

			@Override
			public void onFailed(Throwable throwable) {
				Log.error("Error in add resource: " + throwable.getLocalizedMessage());
				UtilsGXT3.alert("Error", throwable.getLocalizedMessage());
				throwable.printStackTrace();
			}

			@Override
			public void onAborted() {

			}

			@Override
			public void onNotValidSelection() {
				UtilsGXT3.info("Attention", "Select a valid resource!");
			}
		};

		wselectDialog.addWorkspaceExplorerSelectNotificationListener(handler);
		wselectDialog.setZIndex(XDOM.getTopZIndex());
		wselectDialog.show();
	}

	private void addResourceToProject(Item item) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();
		Log.debug("Add Resource To Project Item selected: " + item);
		ItemDescription itemDescription = new ItemDescription(item.getId(), item.getName(), item.getOwner(),
				item.getPath(), item.getType().name());
		StatAlgoImporterServiceAsync.INSTANCE.addResourceToProject(itemDescription, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				monitor.hide();
				fireProjectStatusAddResourceEvent();
			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error in add resource to project: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}
		});

	}

	private void createProjectOnServer(final ProjectCreateSession projectCreateSession) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();
		Log.debug("Create Project: " + projectCreateSession);
		StatAlgoImporterServiceAsync.INSTANCE.createProjectOnWorkspace(projectCreateSession,
				new AsyncCallback<Project>() {

					@Override
					public void onSuccess(Project p) {
						monitor.hide();
						project = p;
						fireWorkAreaSetupEvent();

					}

					@Override
					public void onFailure(Throwable caught) {
						monitor.hide();
						if (caught instanceof StatAlgoImporterSessionExpiredException) {
							eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
						} else {
							Log.error("Error in create project: " + caught.getLocalizedMessage());
							UtilsGXT3.alert("Error", caught.getLocalizedMessage());
						}
						caught.printStackTrace();

					}
				});
	}

	private void openProjectOnServer(Item item) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();

		Log.debug("Open Project Item selected: " + item);
		final ItemDescription newProjectFolder = new ItemDescription(item.getId(), item.getName(), item.getOwner(),
				item.getPath(), item.getType().name());
		StatAlgoImporterServiceAsync.INSTANCE.openProjectOnWorkspace(newProjectFolder, new AsyncCallback<Project>() {

			@Override
			public void onSuccess(Project projectOpened) {
				Log.debug("Open: " + projectOpened);
				monitor.hide();
				project = projectOpened;
				fireWorkAreaSetupEvent();

			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error in open project: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}
		});
	}

	public void saveProject(InputData inputData, final StatAlgoImporterMonitor monitor) {
		Log.debug("Save Project: " + inputData);

		StatAlgoImporterServiceAsync.INSTANCE.saveProject(inputData, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				monitor.hide();
				UtilsGXT3.info("Save", "Project saved!");
				fireProjectStatusExplorerRefreshEvent();
			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error on save project: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}
		});
	}

	public void softwareCreate(final InputData inputData, final StatAlgoImporterMonitor monitor) {
		Log.debug("SoftwareCreate On Server");
		StatAlgoImporterServiceAsync.INSTANCE.createSoftware(inputData, new AsyncCallback<Project>() {

			@Override
			public void onSuccess(Project p) {
				Log.info("Software Created!");
				project = p;
				softwarePublish(monitor);

			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					Log.error("Session Expired");
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error in create software: " + caught.getLocalizedMessage(), caught);
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
					fireProjectStatusExplorerRefreshEvent();
				}

			}
		});

	}

	public void setMainCode(final InputData inputData, final ItemDescription itemDescription) {
		if (project != null) {
			if (project.getMainCode() != null && project.getMainCode().getItemDescription() != null) {
				final ConfirmMessageBox mb = new ConfirmMessageBox("Attention",
						"All previous configurations will be lost. Would you like to change main code?");
				mb.addDialogHideHandler(new DialogHideHandler() {

					@Override
					public void onDialogHide(DialogHideEvent event) {
						switch (event.getHideButton()) {
						case NO:
							mb.hide();
							break;
						case YES:
							mb.hide();
							saveInputDataForMainSet(inputData, itemDescription);
							break;
						default:

							break;
						}

					}
				});
				mb.setWidth(300);
				mb.show();
			} else {
				saveInputDataForMainSet(inputData, itemDescription);
			}

		} else {

		}

	}

	private void saveInputDataForMainSet(final InputData inputData, final ItemDescription itemDescription) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();

		StatAlgoImporterServiceAsync.INSTANCE.saveProject(inputData, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				setMainCodeOnServer(itemDescription, monitor);
				/*
				 * UtilsGXT3.info("Save", "Project saved!");
				 * fireProjectStatusExplorerRefreshEvent();
				 */
			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error on save project: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}
		});
	}

	private void setMainCodeOnServer(final ItemDescription itemDescription, final StatAlgoImporterMonitor monitor) {

		Log.debug("Set Main Code: " + itemDescription);
		StatAlgoImporterServiceAsync.INSTANCE.setMainCode(itemDescription, new AsyncCallback<Project>() {

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error setting main code: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}

			@Override
			public void onSuccess(Project result) {
				monitor.hide();
				project = result;
				fireProjectStatusMainCodeSetEvent();
			}
		});

	}

	public void setNewCode(InputData inputData, NewCodeEvent newCodeEvent, final StatAlgoImporterMonitor monitor) {
		StatAlgoImporterServiceAsync.INSTANCE.setNewCode(inputData, newCodeEvent.getFile(), newCodeEvent.getCode(),
				new AsyncCallback<Project>() {

					@Override
					public void onFailure(Throwable caught) {
						monitor.hide();
						if (caught instanceof StatAlgoImporterSessionExpiredException) {
							eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
						} else {
							Log.error("Error on save new code: " + caught.getLocalizedMessage());
							UtilsGXT3.alert("Error", caught.getLocalizedMessage());
						}
						caught.printStackTrace();

					}

					@Override
					public void onSuccess(Project result) {
						project = result;
						fireProjectStatusMainCodeSetEvent();
						monitor.hide();
					}

				});
	}

	public void setBinaryCode(final InputData inputData, final ItemDescription itemDescription) {
		if (project != null) {
			if (project.getProjectConfig() != null && project.getProjectConfig().getProjectSupport() != null
					&& (project.getProjectConfig().getProjectSupport() instanceof ProjectSupportBlackBox
							|| project.getProjectConfig().getProjectSupport() instanceof ProjectSupportBashEdit)) {
				saveInputDataForBinarySet(inputData, itemDescription);
			}

		} else {

		}

	}

	private void saveInputDataForBinarySet(final InputData inputData, final ItemDescription itemDescription) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();

		StatAlgoImporterServiceAsync.INSTANCE.saveProject(inputData, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				setBinaryCodeOnServer(itemDescription, monitor);
			}

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error on save project: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}
		});
	}

	private void setBinaryCodeOnServer(final ItemDescription itemDescription, final StatAlgoImporterMonitor monitor) {

		Log.debug("Set Binary Code: " + itemDescription);
		StatAlgoImporterServiceAsync.INSTANCE.setBinaryCode(itemDescription, new AsyncCallback<Project>() {

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error setting code: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();

			}

			@Override
			public void onSuccess(Project result) {
				monitor.hide();
				project = result;
				fireProjectStatusBinaryCodeSetEvent();
			}
		});

	}

	public void deleteItem(final ItemDescription itemDescription) {
		if (project != null && project.getMainCode() != null && project.getMainCode().getItemDescription() != null
				&& itemDescription.getId().compareTo(project.getMainCode().getItemDescription().getId()) == 0) {
			final ConfirmMessageBox mb = new ConfirmMessageBox("Delete",
					"All previous configurations will be lost. Would you like to delete main code?");
			mb.addDialogHideHandler(new DialogHideHandler() {

				@Override
				public void onDialogHide(DialogHideEvent event) {
					switch (event.getHideButton()) {
					case NO:
						break;
					case YES:
						deleteItemOnServer(itemDescription, true);
						break;
					default:
						break;
					}

				}
			});
			mb.setWidth(300);
			mb.show();

		} else {
			final ConfirmMessageBox mb = new ConfirmMessageBox("Delete", "Would you like to delete this resource?");
			mb.addDialogHideHandler(new DialogHideHandler() {

				@Override
				public void onDialogHide(DialogHideEvent event) {
					switch (event.getHideButton()) {
					case NO:
						break;
					case YES:
						deleteItemOnServer(itemDescription, false);
						break;
					default:
						break;
					}

				}

			});
			mb.setWidth(300);
			mb.show();

		}

	}

	private void deleteItemOnServer(ItemDescription itemDescription, final boolean mainCode) {
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();

		StatAlgoImporterServiceAsync.INSTANCE.deleteResourceOnProject(itemDescription, new AsyncCallback<Project>() {

			@Override
			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					Log.error("Error deleting resourse: " + caught.getLocalizedMessage());
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
				}
				caught.printStackTrace();
			}

			@Override
			public void onSuccess(Project result) {
				monitor.hide();
				project = result;
				if (mainCode) {
					fireProjectStatusDeleteMainCodeEvent();
				} else {
					fireProjectStatusDeleteEvent();
				}
			}
		});

	}

	public void softwareRepackage() {
		if (project != null) {
			final ConfirmMessageBox mb = new ConfirmMessageBox("Attention",
					"This operation repackages the script only without recreating the Java software. The already deployed Java software will use the new script package. Do you want to proceed?");
			mb.addDialogHideHandler(new DialogHideHandler() {

				@Override
				public void onDialogHide(DialogHideEvent event) {
					switch (event.getHideButton()) {
					case NO:
						break;
					case YES:
						softwareRepackageOnServer();
						break;
					default:
						break;
					}

				}
			});
			mb.setWidth(300);
			mb.show();

		} else {
			Log.error("Project not open: " + project);
			UtilsGXT3.alert("Error", "Project not open!");
		}

	}

	private void softwareRepackageOnServer() {
		Log.info("Software Repackage On Server");
		final StatAlgoImporterMonitor monitor = new StatAlgoImporterMonitor();
		StatAlgoImporterServiceAsync.INSTANCE.repackageSoftware(new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
					fireProjectStatusExplorerRefreshEvent();
				}
			}

			public void onSuccess(Void result) {
				monitor.hide();
				UtilsGXT3.info("Repackage", "The script has been repackaged!");
				fireProjectStatusSoftwareRepackageEvent();
			}

		});
	}

	private void softwarePublish(final StatAlgoImporterMonitor monitor) {
		Log.debug("SoftwarePublish invocation");
		if (project != null) {
			softwarePublishOnServer(monitor);

		} else {
			monitor.hide();
			Log.error("Project not open: " + project);
			UtilsGXT3.alert("Error", "Project not open!");
		}

	}

	// TODO
	private void softwarePublishOnServer(final StatAlgoImporterMonitor monitor) {
		Log.info("SoftwarePublish On Server");
		StatAlgoImporterServiceAsync.INSTANCE.publishSoftware(new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
					fireProjectStatusExplorerRefreshEvent();

				}
			}

			public void onSuccess(String operationId) {
				Log.debug("Publish result: " + operationId);
				if (saiDescriptor != null && saiDescriptor.getPoolManagerConfig() != null
						&& saiDescriptor.getPoolManagerConfig().isEnable()) {
					if (operationId != null && !operationId.isEmpty()) {
						monitorOperationDeployStatus(operationId, monitor);
					} else {
						monitor.hide();
						Log.info(
								"Publish, the software has been published, but no autodeploy on server is enable so some time is required for installation by administrators!");
						UtilsGXT3.info("Publish",
								"The software has been published! The Cloud system is being updated. Your process will be available on the DataMiner system within some minutes by administrators.");
						fireProjectStatusSoftwarePublishEvent();
					}
				} else {
					monitor.hide();
					UtilsGXT3.info("Publish",
							"The software has been published! The Cloud system is being updated. Your process will be available on the DataMiner system within some minutes by administrators.");
					fireProjectStatusSoftwarePublishEvent();
				}
			}

		});
	}

	// TODO
	private void monitorOperationDeployStatus(final String operationId, final StatAlgoImporterMonitor monitor) {
		final MonitorDeployOperation monitorDeployOperation = new MonitorDeployOperation();
		MonitorDeployOperationEventHandler handler = new MonitorDeployOperationEventHandler() {

			@Override
			public void onMonitor(MonitorDeployOperationEvent event) {
				StatAlgoImporterServiceAsync.INSTANCE.getDeployOperationStatus(operationId,
						new AsyncCallback<String>() {

							public void onFailure(Throwable caught) {
								monitorDeployOperation.stop();
								monitor.hide();
								if (caught instanceof StatAlgoImporterSessionExpiredException) {
									eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
								} else {
									UtilsGXT3.alert("Error", caught.getLocalizedMessage());
									fireProjectStatusExplorerRefreshEvent();
								}
							}

							public void onSuccess(String deployOperationStatus) {
								Log.debug("Deploy Operation Status: " + deployOperationStatus);
								if (deployOperationStatus == null) {
									monitorDeployOperation.repeat();
								}

								switch (deployOperationStatus) {
								case "COMPLETED":
									monitorDeployOperation.stop();
									monitor.hide();
									UtilsGXT3.info("Publish",
											"The software has been published! The Cloud system is being updated. Your process will be available on the DataMiner system within some minutes.");
									fireProjectStatusSoftwarePublishEvent();
									break;
								case "FAILED":
									monitorDeployOperation.stop();
									retrieveOperationDeployLogs(operationId, monitor);

									break;
								case "IN PROGRESS":
									monitorDeployOperation.repeat();
									break;
								default:
									monitorDeployOperation.repeat();
									break;

								}

							}

						});
			}
		};

		monitorDeployOperation.addHandler(handler);
		monitorDeployOperation.start();

	}

	private void retrieveOperationDeployLogs(final String operationId, final StatAlgoImporterMonitor monitor) {
		StatAlgoImporterServiceAsync.INSTANCE.getDeployOperationLogs(operationId, new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {

				monitor.hide();
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());
					fireProjectStatusExplorerRefreshEvent();
				}
			}

			public void onSuccess(String logs) {
				Log.debug("Deploy Operation Logs: " + logs);
				monitor.hide();
				UtilsGXT3.alert("Error", "Error deploying the algorithm: <a href='" + logs + "'>Logs</a>");
				fireProjectStatusExplorerRefreshEvent();

			}

		});

	}

	public void startProjectManager() {
		retrieveSAIDescriptor(null);
	}

	public void startProjectManager(String restoreValue) {
		retrieveSAIDescriptor(restoreValue);

	}

	private void retrieveSAIDescriptor(final String restoreValue) {
		StatAlgoImporterServiceAsync.INSTANCE.getSAIDescriptor(new AsyncCallback<SAIDescriptor>() {

			@Override
			public void onFailure(Throwable caught) {
				Log.info("Error retrieving SAI descriptor: " + caught.getMessage(), caught);
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					UtilsGXT3.alert("Error", "Error retrieving SAI descriptor: " + caught.getLocalizedMessage());
				}
			}

			@Override
			public void onSuccess(SAIDescriptor saiDesc) {
				saiDescriptor = saiDesc;
				Log.info("SAIDescriptor: " + saiDescriptor);
				if (restoreValue == null || restoreValue.isEmpty()) {
					fireProjectStatusStartEvent();
				} else {
					restoreProject(restoreValue);
				}

			}

		});

	}

	private void restoreProject(String restoreValue) {
		Log.info("Restore Project");
		StatAlgoImporterServiceAsync.INSTANCE.restoreUISession(restoreValue, new AsyncCallback<Project>() {

			public void onFailure(Throwable caught) {
				if (caught instanceof StatAlgoImporterSessionExpiredException) {
					eventBus.fireEvent(new SessionExpiredEvent(SessionExpiredType.EXPIREDONSERVER));
				} else {
					UtilsGXT3.alert("Error", caught.getLocalizedMessage());

				}
			}

			public void onSuccess(Project p) {
				if (p != null) {
					project = p;
					fireWorkAreaSetupEvent();
				}
			}

		});

	}

	private void fireWorkAreaSetupEvent() {
		WorkAreaEvent workAreaEvent = new WorkAreaEvent(WorkAreaEventType.WORK_AREA_SETUP, project);
		Log.debug("Project Manager workAreaEvent fire! " + workAreaEvent);
		eventBus.fireEvent(workAreaEvent);

	}

	@SuppressWarnings("unused")
	private void fireProjectStatusSaveProjectEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.SAVE, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);

	}

	private void fireProjectStatusAddResourceEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.ADD_RESOURCE, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	private void fireProjectStatusDeleteEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.DELETE_RESOURCE, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	private void fireProjectStatusDeleteMainCodeEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.DELETE_MAIN_CODE,
				project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	@SuppressWarnings("unused")
	private void fireProjectStatusUpdateEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.UPDATE, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	private void fireProjectStatusExplorerRefreshEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.EXPLORER_REFRESH,
				project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);

	}

	private void fireProjectStatusMainCodeSetEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.MAIN_CODE_SET, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	private void fireProjectStatusBinaryCodeSetEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.BINARY_CODE_SET, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	/*
	 * private void fireProjectStatusSoftwareCreatedEvent() { ProjectStatusEvent
	 * projectStatusEvent = new
	 * ProjectStatusEvent(ProjectStatusEventType.SOFTWARE_CREATED, project);
	 * Log.debug("Project Manager ProjectStatusEvent fire! " +
	 * projectStatusEvent); eventBus.fireEvent(projectStatusEvent);
	 * 
	 * }
	 */

	private void fireProjectStatusSoftwarePublishEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.SOFTWARE_PUBLISH,
				project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);

	}

	private void fireProjectStatusSoftwareRepackageEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.SOFTWARE_REPACKAGE,
				project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);
	}

	private void fireProjectStatusStartEvent() {
		ProjectStatusEvent projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.START, project);
		Log.debug("Project Manager ProjectStatusEvent fire! " + projectStatusEvent);
		eventBus.fireEvent(projectStatusEvent);

	}

	/*
	 * private void fireProjectStatusOpenEvent() { ProjectStatusEvent
	 * projectStatusEvent = new ProjectStatusEvent(ProjectStatusEventType.OPEN,
	 * project); Log.debug("Project Manager ProjectStatusEvent fire! " +
	 * projectStatusEvent); eventBus.fireEvent(projectStatusEvent);
	 * 
	 * }
	 */

}
