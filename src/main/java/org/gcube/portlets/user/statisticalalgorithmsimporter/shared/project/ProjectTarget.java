package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectTarget implements Serializable {

	private static final long serialVersionUID = 480665662744105383L;
	private ItemDescription folder;
	private ProjectCompile projectCompile;
	private ProjectDeploy projectDeploy;

	public ProjectTarget() {
		super();
	}

	public ProjectTarget(ItemDescription folder) {
		super();
		this.folder = folder;
	}

	public ItemDescription getFolder() {
		return folder;
	}

	public void setFolder(ItemDescription folder) {
		this.folder = folder;
	}

	public ProjectCompile getProjectCompile() {
		return projectCompile;
	}

	public void setProjectCompile(ProjectCompile projectCompile) {
		this.projectCompile = projectCompile;
	}

	public ProjectDeploy getProjectDeploy() {
		return projectDeploy;
	}

	public void setProjectDeploy(ProjectDeploy projectDeploy) {
		this.projectDeploy = projectDeploy;
	}

	@Override
	public String toString() {
		return "ProjectTarget [folder=" + folder + ", projectCompile="
				+ projectCompile + ", projectDeploy=" + projectDeploy + "]";
	}

	

}
