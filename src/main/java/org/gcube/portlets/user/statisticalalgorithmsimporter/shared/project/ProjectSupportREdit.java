package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectSupportREdit implements ProjectSupport {

	private static final long serialVersionUID = -5337735100116549017L;

	public ProjectSupportREdit() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectSupportREdit []";
	}

}
