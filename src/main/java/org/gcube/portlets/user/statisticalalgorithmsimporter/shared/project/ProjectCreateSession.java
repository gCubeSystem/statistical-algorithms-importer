package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import java.io.Serializable;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.descriptor.ProjectSetup;
import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectCreateSession implements Serializable {

	private static final long serialVersionUID = -6892840268452906937L;

	private ItemDescription newProjectFolder;
	private ProjectSetup projectSetup;

	public ProjectCreateSession() {
		super();
	}

	public ProjectCreateSession(ItemDescription newProjectFolder, ProjectSetup projectSetup) {
		super();
		this.newProjectFolder = newProjectFolder;
		this.projectSetup = projectSetup;
	}

	public ItemDescription getNewProjectFolder() {
		return newProjectFolder;
	}

	public void setNewProjectFolder(ItemDescription newProjectFolder) {
		this.newProjectFolder = newProjectFolder;
	}

	public ProjectSetup getProjectSetup() {
		return projectSetup;
	}

	public void setProjectSetup(ProjectSetup projectSetup) {
		this.projectSetup = projectSetup;
	}

	@Override
	public String toString() {
		return "ProjectCreateSession [newProjectFolder=" + newProjectFolder + ", projectSetup=" + projectSetup + "]";
	}

}
