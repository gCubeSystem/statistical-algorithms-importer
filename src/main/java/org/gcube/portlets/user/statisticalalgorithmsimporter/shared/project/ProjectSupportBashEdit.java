package org.gcube.portlets.user.statisticalalgorithmsimporter.shared.project;

import org.gcube.portlets.user.statisticalalgorithmsimporter.shared.workspace.ItemDescription;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ProjectSupportBashEdit implements ProjectSupport {

	private static final long serialVersionUID = 7394036864125922191L;

	private ItemDescription binaryItem;

	public ProjectSupportBashEdit() {
		super();
	}

	/**
	 * 
	 * @param binaryItem
	 *            Binary Item
	 */
	public ProjectSupportBashEdit(ItemDescription binaryItem) {
		super();
		this.binaryItem = binaryItem;

	}

	public ItemDescription getBinaryItem() {
		return binaryItem;
	}

	public void setBinaryItem(ItemDescription binaryItem) {
		this.binaryItem = binaryItem;
	}

	@Override
	public String toString() {
		return "ProjectSupportBashEdit [binaryItem=" + binaryItem + "]";
	}

}
