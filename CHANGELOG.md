This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "statistical-algorithms-importer"



## [v1.16.0]

 - Updated to maven-portal-bom-4.0.0 
 


## [v1.15.0] - 2021-10-06

 - Added cluster description in Service Info [#19213]



## [v1.14.1] - 2020-05-20

- Updated GitHub client library [#18318]



## [v1.14.0] - 2019-10-01

- Added service info [#12594]
- Added Item Id support [#16503] 
- Updated the resources check on IS [#17569] 



## [v1.13.2] - 2019-02-26

- Updated resource selection how workaround for WorkspaceExplorer widget [#16157] 



## [v1.13.1] - 2018-12-06

- Added the project folder path update in the open operation [#12977] 



## [v1.13.0] - 2018-10-01

- Updated support to StorageHub [#11724] 



## [v1.12.0] - 2018-07-01

- Updated to StorageHub [#11724] 



## [v1.11.0] - 2018-06-01

- Added Status control in Algorithm Generator [#11750]
- Added System parameters support [#11768] 



## [v1.10.0] - 2018-01-12

- Added support to Private algorithms [#10779]
- Added user name to algorithms descriptions in SAI publication [#10705]



## [v1.9.0] - 2017-12-13

- Fixed Set Main behavior in case of R [#10523] 
- Only publish button is available [#10187]



## [v1.8.0] - 2017-11-09

- Updated Tools panel



## [v1.7.1] - 2017-10-01

- Fixed Notification service retrieval



## [v1.7.0] - 2017-07-01

- Added support for Processes as Black Boxes [#8819]



## [v1.6.0] - 2017-06-12

- Support Java 8 compatibility [#8541] 



## [v1.5.0] - 2017-02-15

- Updated PortalContext support [#6279]
- Added support only for file data type in output parameter [#7120]



## [v1.4.0] - 2016-12-01

- Updated Storage support
- Added PortalContext



## [v1.3.0] - 2016-11-01

- SAI enhancements [#4896]
- Updated to Auth 2.0



## [v1.2.0] - 2016-10-01

- Update to Liferay 6.2
- Added GitHub Connector



## [v1.1.0] - 2016-05-01

- Fixed back button behavior [#3283] 



## [v1.0.0] - 2016-02-28

- First Release



